From 08813ddb99c2f923ef9cfb3167caec524794e79b Mon Sep 17 00:00:00 2001
From: Vijaya Kumar K <vkilari@codeaurora.org>
Date: Tue, 7 Nov 2017 08:41:36 +0000
Subject: [PATCH] Redis optimization: Avoid memmove() after processing every
 command

In case of pipelining, multiple commands are queued up in query buffer.
After processing each command, memmove() is called from sdsrange() function
which moves the next command in the buffer to the beginning of the querybuf.

This memmove() consumes ~10% of the time with higher P values.
This memmove() can be avoided if index of next command to be processed is stored
in a variable (qbufpos). The index variable is updated after processing every command
and reset when the query buffer is refilled with the new request.

The optimization seen with these changes is significant.
With the below command on Intel machine, we see 15% improvement.
On other architecture where memory operations are slow will see significant
improvement in redis benchmarks.

With the below command we see 18% improvement

redis-benchmark -h 192.168.3.100 -p 10001 -c 50 -n 200000 -d 10 -k 1 -r 10000 -P 100 get

---------------------------------------------------------------------------------
Without optimization (reqs/sec)  | With optimization (reqs/sec) | Improvement % |
---------------------------------------------------------------------------------
                        2507037  |                   2962205    |  18.2 %       |
---------------------------------------------------------------------------------

Signed-off-by: Vijaya Kumar K <vkilari@codeaurora.org>

diff --git a/networking.c b/networking.c
index aeaeca9..8b67610 100644
--- a/networking.c
+++ b/networking.c
@@ -103,6 +103,7 @@ client *createClient(int fd) {
     c->argv = NULL;
     c->cmd = c->lastcmd = NULL;
     c->multibulklen = 0;
+    c->qbufpos = 0;
     c->bulklen = -1;
     c->sentlen = 0;
     c->flags = 0;
@@ -1050,7 +1051,7 @@ int processInlineBuffer(client *c) {
     size_t querylen;
 
     /* Search for end of line */
-    newline = strchr(c->querybuf,'\n');
+    newline = strchr(c->querybuf+c->qbufpos,'\n');
 
     /* Nothing to do without a \r\n */
     if (newline == NULL) {
@@ -1062,12 +1063,12 @@ int processInlineBuffer(client *c) {
     }
 
     /* Handle the \r\n case. */
-    if (newline && newline != c->querybuf && *(newline-1) == '\r')
+    if (newline && newline != (c->querybuf+c->qbufpos) && *(newline-1) == '\r')
         newline--;
 
     /* Split the input buffer up to the \r\n */
-    querylen = newline-(c->querybuf);
-    aux = sdsnewlen(c->querybuf,querylen);
+    querylen = newline-(c->querybuf+c->qbufpos);
+    aux = sdsnewlen(c->querybuf+c->qbufpos,querylen);
     argv = sdssplitargs(aux,&argc);
     sdsfree(aux);
     if (argv == NULL) {
@@ -1082,9 +1083,10 @@ int processInlineBuffer(client *c) {
     if (querylen == 0 && c->flags & CLIENT_SLAVE)
         c->repl_ack_time = server.unixtime;
 
-    /* Leave data after the first line of the query in the buffer */
-    sdsrange(c->querybuf,querylen+2,-1);
+    /* Update querybuf length with new length */
+    sdstrimlen(c->querybuf,querylen+2);
 
+    c->qbufpos += querylen+2;
     /* Setup argv array on client structure */
     if (argc) {
         if (c->argv) zfree(c->argv);
@@ -1114,9 +1116,9 @@ static void setProtocolError(const char *errstr, client *c, int pos) {
         /* Sample some protocol to given an idea about what was inside. */
         char buf[256];
         if (sdslen(c->querybuf) < PROTO_DUMP_LEN) {
-            snprintf(buf,sizeof(buf),"Query buffer during protocol error: '%s'", c->querybuf);
+            snprintf(buf,sizeof(buf),"Query buffer during protocol error: '%s'", c->querybuf+c->qbufpos);
         } else {
-            snprintf(buf,sizeof(buf),"Query buffer during protocol error: '%.*s' (... more %zu bytes ...) '%.*s'", PROTO_DUMP_LEN/2, c->querybuf, sdslen(c->querybuf)-PROTO_DUMP_LEN, PROTO_DUMP_LEN/2, c->querybuf+sdslen(c->querybuf)-PROTO_DUMP_LEN/2);
+            snprintf(buf,sizeof(buf),"Query buffer during protocol error: '%.*s' (... more %zu bytes ...) '%.*s'", PROTO_DUMP_LEN/2, c->querybuf+c->qbufpos, sdslen(c->querybuf)-PROTO_DUMP_LEN, PROTO_DUMP_LEN/2, c->querybuf+c->qbufpos+sdslen(c->querybuf)-PROTO_DUMP_LEN/2);
         }
 
         /* Remove non printable chars. */
@@ -1132,7 +1134,7 @@ static void setProtocolError(const char *errstr, client *c, int pos) {
         sdsfree(client);
     }
     c->flags |= CLIENT_CLOSE_AFTER_REPLY;
-    sdsrange(c->querybuf,pos,-1);
+    sdstrimlen(c->querybuf,pos);
 }
 
 /* Process the query buffer for client 'c', setting up the client argument
@@ -1156,7 +1158,7 @@ int processMultibulkBuffer(client *c) {
         serverAssertWithInfo(c,NULL,c->argc == 0);
 
         /* Multi bulk length cannot be read without a \r\n */
-        newline = strchr(c->querybuf,'\r');
+        newline = strchr(c->querybuf+c->qbufpos,'\r');
         if (newline == NULL) {
             if (sdslen(c->querybuf) > PROTO_INLINE_MAX_SIZE) {
                 addReplyError(c,"Protocol error: too big mbulk count string");
@@ -1166,22 +1168,26 @@ int processMultibulkBuffer(client *c) {
         }
 
         /* Buffer should also contain \n */
-        if (newline-(c->querybuf) > ((signed)sdslen(c->querybuf)-2))
+        if (newline-(c->querybuf+c->qbufpos) > ((signed)sdslen(c->querybuf)-2))
             return C_ERR;
 
         /* We know for sure there is a whole line since newline != NULL,
          * so go ahead and find out the multi bulk length. */
-        serverAssertWithInfo(c,NULL,c->querybuf[0] == '*');
-        ok = string2ll(c->querybuf+1,newline-(c->querybuf+1),&ll);
+        serverAssertWithInfo(c,NULL,c->querybuf[0+c->qbufpos] == '*');
+        ok = string2ll(c->querybuf+1+c->qbufpos,newline-(c->querybuf+c->qbufpos +1),&ll);
         if (!ok || ll > 1024*1024) {
             addReplyError(c,"Protocol error: invalid multibulk length");
             setProtocolError("invalid mbulk count",c,pos);
             return C_ERR;
         }
 
-        pos = (newline-c->querybuf)+2;
+        pos = (newline-(c->querybuf+c->qbufpos))+2;
         if (ll <= 0) {
-            sdsrange(c->querybuf,pos,-1);
+            /* Buffer does not have sufficient data. So move the remaining contents to
+             * beginning of the query buffer and update length. */
+            memmove(c->querybuf, c->querybuf+c->qbufpos+pos, sdslen(c->querybuf)-pos); 
+            sdstrimlen(c->querybuf,pos);
+            c->qbufpos=0;
             return C_OK;
         }
 
@@ -1196,7 +1202,7 @@ int processMultibulkBuffer(client *c) {
     while(c->multibulklen) {
         /* Read bulk length if unknown */
         if (c->bulklen == -1) {
-            newline = strchr(c->querybuf+pos,'\r');
+            newline = strchr(c->querybuf+c->qbufpos+pos,'\r');
             if (newline == NULL) {
                 if (sdslen(c->querybuf) > PROTO_INLINE_MAX_SIZE) {
                     addReplyError(c,
@@ -1204,37 +1210,52 @@ int processMultibulkBuffer(client *c) {
                     setProtocolError("too big bulk count string",c,0);
                     return C_ERR;
                 }
+                /* Buffer is split. Before reading the next contigous buffer. sdsMakeRoomFor() will
+                 * copy the buffer from index 0. So move the buffer before going from here.
+                 */
+                memmove(c->querybuf, c->querybuf+c->qbufpos+pos, sdslen(c->querybuf)-pos);
+                c->qbufpos=0;
                 break;
             }
 
             /* Buffer should also contain \n */
-            if (newline-(c->querybuf) > ((signed)sdslen(c->querybuf)-2))
+            if (newline-(c->querybuf+c->qbufpos) > ((signed)sdslen(c->querybuf)-2)) {
+                /* Before Reading the next contigous buffer. sdsMakeRoomFor() will
+                 * copy the buffer from index 0. So move the buffer before going from here.
+                 */
+                memmove(c->querybuf, c->querybuf+c->qbufpos+pos, sdslen(c->querybuf)-pos);
+                c->qbufpos=0;
                 break;
+            }
 
-            if (c->querybuf[pos] != '$') {
+            if (c->querybuf[pos+c->qbufpos] != '$') {
                 addReplyErrorFormat(c,
                     "Protocol error: expected '$', got '%c'",
-                    c->querybuf[pos]);
+                    c->querybuf[pos+c->qbufpos]);
                 setProtocolError("expected $ but got something else",c,pos);
                 return C_ERR;
             }
 
-            ok = string2ll(c->querybuf+pos+1,newline-(c->querybuf+pos+1),&ll);
+            ok = string2ll(c->querybuf+c->qbufpos+pos+1,newline-(c->querybuf+c->qbufpos+pos+1),&ll);
             if (!ok || ll < 0 || ll > 512*1024*1024) {
                 addReplyError(c,"Protocol error: invalid bulk length");
                 setProtocolError("invalid bulk length",c,pos);
                 return C_ERR;
             }
 
-            pos += newline-(c->querybuf+pos)+2;
+            pos += newline-(c->querybuf+c->qbufpos+pos)+2;
             if (ll >= PROTO_MBULK_BIG_ARG) {
                 size_t qblen;
 
+                /* Move remaining contents of the beginning of the buffer before attempt to
+                 * read from network. */
+                memmove(c->querybuf, c->querybuf+c->qbufpos+pos, sdslen(c->querybuf)-pos);
+                sdstrimlen(c->querybuf,sdslen(c->querybuf)-pos);
+                c->qbufpos=0;
                 /* If we are going to read a large object from network
                  * try to make it likely that it will start at c->querybuf
                  * boundary so that we can optimize object creation
                  * avoiding a large copy of data. */
-                sdsrange(c->querybuf,pos,-1);
                 pos = 0;
                 qblen = sdslen(c->querybuf);
                 /* Hint the sds library about the amount of bytes this string is
@@ -1248,6 +1269,8 @@ int processMultibulkBuffer(client *c) {
         /* Read bulk argument */
         if (sdslen(c->querybuf)-pos < (unsigned)(c->bulklen+2)) {
             /* Not enough data (+2 == trailing \r\n) */
+            memmove(c->querybuf, c->querybuf+c->qbufpos+pos, sdslen(c->querybuf)-pos);
+            c->qbufpos=0;
             break;
         } else {
             /* Optimization: if the buffer contains JUST our bulk element
@@ -1257,16 +1280,17 @@ int processMultibulkBuffer(client *c) {
                 c->bulklen >= PROTO_MBULK_BIG_ARG &&
                 (signed) sdslen(c->querybuf) == c->bulklen+2)
             {
-                c->argv[c->argc++] = createObject(OBJ_STRING,c->querybuf);
+                c->argv[c->argc++] = createObject(OBJ_STRING,c->querybuf+c->qbufpos);
                 sdsIncrLen(c->querybuf,-2); /* remove CRLF */
                 /* Assume that if we saw a fat argument we'll see another one
                  * likely... */
                 c->querybuf = sdsnewlen(NULL,c->bulklen+2);
                 sdsclear(c->querybuf);
                 pos = 0;
+                c->qbufpos=0;
             } else {
                 c->argv[c->argc++] =
-                    createStringObject(c->querybuf+pos,c->bulklen);
+                    createStringObject(c->querybuf+c->qbufpos+pos,c->bulklen);
                 pos += c->bulklen+2;
             }
             c->bulklen = -1;
@@ -1275,7 +1299,8 @@ int processMultibulkBuffer(client *c) {
     }
 
     /* Trim to pos */
-    if (pos) sdsrange(c->querybuf,pos,-1);
+    c->qbufpos += pos;
+    if (pos) sdstrimlen(c->querybuf,pos);
 
     /* We're done when c->multibulk == 0 */
     if (c->multibulklen == 0) return C_OK;
@@ -1290,6 +1315,7 @@ int processMultibulkBuffer(client *c) {
  * pending query buffer, already representing a full command, to process. */
 void processInputBuffer(client *c) {
     server.current_client = c;
+    c->qbufpos = 0;
     /* Keep processing while there is something in the input buffer */
     while(sdslen(c->querybuf)) {
         /* Return if clients are paused. */
@@ -1307,7 +1333,7 @@ void processInputBuffer(client *c) {
 
         /* Determine request type when unknown. */
         if (!c->reqtype) {
-            if (c->querybuf[0] == '*') {
+            if (c->querybuf[0+c->qbufpos] == '*') {
                 c->reqtype = PROTO_REQ_MULTIBULK;
             } else {
                 c->reqtype = PROTO_REQ_INLINE;
diff --git a/sds.c b/sds.c
index eafa13c..0b64ecd 100644
--- a/sds.c
+++ b/sds.c
@@ -742,6 +742,15 @@ void sdsrange(sds s, int start, int end) {
     sdssetlen(s,newlen);
 }
 
+void sdstrimlen(sds s, int len) {
+    size_t newlen;
+
+    if (len == 0 || (len > (int)sdslen(s))) return;
+
+    newlen = (int)sdslen(s) - len;
+    sdssetlen(s, newlen);
+}
+
 /* Apply tolower() to every character of the sds string 's'. */
 void sdstolower(sds s) {
     int len = sdslen(s), j;
diff --git a/sds.h b/sds.h
index 394f8b5..6407d37 100644
--- a/sds.h
+++ b/sds.h
@@ -237,6 +237,7 @@ sds sdscatprintf(sds s, const char *fmt, ...);
 sds sdscatfmt(sds s, char const *fmt, ...);
 sds sdstrim(sds s, const char *cset);
 void sdsrange(sds s, int start, int end);
+void sdstrimlen(sds s, int len);
 void sdsupdatelen(sds s);
 void sdsclear(sds s);
 int sdscmp(const sds s1, const sds s2);
diff --git a/server.h b/server.h
index e3b5607..bed4e32 100644
--- a/server.h
+++ b/server.h
@@ -722,6 +722,7 @@ typedef struct client {
     dict *pubsub_channels;  /* channels a client is interested in (SUBSCRIBE) */
     list *pubsub_patterns;  /* patterns a client is interested in (SUBSCRIBE) */
     sds peerid;             /* Cached peer ID. */
+    int qbufpos;            /* Current query buffer command position */
 
     /* Response buffer */
     int bufpos;
-- 
1.8.3.1

